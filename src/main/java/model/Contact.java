package model;

import java.util.Date;
import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "contact", catalog = "revegyenterprise_4_0_qa")
public class Contact implements Serializable, Comparable {
    public Contact() {
        lastUpdated = new java.util.Date();
    }

    private ContactId id;
    private String name = "";
    private String shortName = "";
    private String contactTypeKey = "";
    private String title = "";
    private String email = "";
    private String phone = "";
    private String mobile = "";
    private String company = "";
    private String loginID = "";
    private boolean deleted = false;
    private java.util.Date lastUpdated;
    private String lastUpdatedBy;
    private String userDefinedField1;
    private String userDefinedField2;
    private String CRMXREF;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "companyId", column = @Column(name = "CompanyID", nullable = false, length = 32)),
            @AttributeOverride(name = "key", column = @Column(name = "\"Key\"", nullable = false, length = 36)) })
    public ContactId getId() {
        return this.id;
    }

    public void setId(ContactId id) {
        this.id = id;
    }

    @Column(name = "Name", length = 32)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "ShortName", length = 8)
    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Column(name = "Email", length = 64)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "Phone", length = 24)
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "Company", length = 32)
    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Column(name = "loginID", length = 36)
    public String getLoginID() {
        return this.loginID;
    }

    public void setLoginID(String loginId) {
        this.loginID = loginId;
    }

    @Column(name = "ContactTypeKey", length = 36)
    public String getContactTypeKey() {
        return this.contactTypeKey;
    }

    public void setContactTypeKey(String contactTypeKey) {
        this.contactTypeKey = contactTypeKey;
    }

    @Column(name = "Deleted", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isDeleted() {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastUpdated", length = 19)
    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "LastUpdatedBy", length = 100)
    public String getLastUpdatedBy() {
        return this.lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    @Column(name = "Mobile", length = 24)
    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Column(name = "Title", length = 100)
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "UserDefinedField1", length = 100)
    public String getUserDefinedField1() {
        return this.userDefinedField1;
    }

    public void setUserDefinedField1(String userDefinedField1) {
        this.userDefinedField1 = userDefinedField1;
    }

    @Column(name = "UserDefinedField2", length = 100)
    public String getUserDefinedField2() {
        return this.userDefinedField2;
    }

    public void setUserDefinedField2(String userDefinedField2) {
        this.userDefinedField2 = userDefinedField2;
    }

    @Column(name = "CRMXREF", length = 100)
    public String getCRMXREF() {
        return this.CRMXREF;
    }

    public void setCRMXREF(String crmxref) {
        this.CRMXREF = crmxref;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Contact))
            return 1;
        Contact c = (Contact) o;
        if (name == null && c.name == null) {
            return 0;
        } else if (name == null) {
            return -1;
        } else if (c.name == null) {
            return 1;
        }
        return name.compareToIgnoreCase(c.name);
    }

}

package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "reportinggroupplaybookconfig", catalog = "revegyenterprise_4_0_qa")
public class ReportingGroupPlaybookConfig implements Serializable {

    private String ID;
    private String playbookConfigKey;
    private ReportingGroup reportinggroup;

    public ReportingGroupPlaybookConfig() {
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "PlaybookConfigKey")
    public String getPlaybookConfigKey() {
        return playbookConfigKey;
    }

    public void setPlaybookConfigKey(String playbookConfigKey) {
        this.playbookConfigKey = playbookConfigKey;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReportingGroupID")
    public ReportingGroup getReportinggroup() {
        return this.reportinggroup;
    }

    public void setReportinggroup(ReportingGroup reportinggroup) {
        this.reportinggroup = reportinggroup;
    }

}

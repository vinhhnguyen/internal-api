package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "reportinggroupmembership", catalog = "revegyenterprise_4_0_qa")
public class ReportingGroupMembership implements Serializable {

    public ReportingGroupMembership() {
    }

    private String ID;
    private String loginID;
    private int type;
    private ReportingGroup reportinggroup;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "LoginID")
    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    @Column(name = "Type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReportingGroupID")
    public ReportingGroup getReportinggroup() {
        return this.reportinggroup;
    }

    public void setReportinggroup(ReportingGroup reportinggroup) {
        this.reportinggroup = reportinggroup;
    }

}

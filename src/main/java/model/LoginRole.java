package model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "loginrole", catalog = "revegyenterprise_4_0_qa")
public class LoginRole implements Serializable {
    public LoginRole() {
    }

    private LoginRoleId id;
    private String name;
    private String description;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "companyId", column = @Column(name = "CompanyID", nullable = false, length = 36)),
            @AttributeOverride(name = "key", column = @Column(name = "Key", nullable = false, length = 36)) })
    public LoginRoleId getId() {
        return this.id;
    }

    public void setId(LoginRoleId id) {
        this.id = id;
    }

    @Column(name = "Name", length = 80)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Description", length = 100)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean equals(Object obj) {
        return (this == obj);
    }

    public int hashCode() {
        return super.hashCode();
    }
}

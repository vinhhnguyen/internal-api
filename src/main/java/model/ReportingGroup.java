package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement()
@Entity
@Table(name = "reportinggroup", catalog = "revegyenterprise_4_0_qa")
public class ReportingGroup implements Serializable {

    public ReportingGroup() {
    }

    private String ID;
    private String companyID;
    private String groupName;
    private String parentGroupID;
    private String primaryLoginID;
    private String integratedPluginKey;
    private String CRMXREF;
    private Set<ReportingGroupGridConfig> reportingGroupGridConfigSet = new HashSet<>();
    private Set<ReportingGroupMapConfig> reportingGroupMapConfigSet = new HashSet<>();
    private Set<ReportingGroupMembership> reportingGroupMembershipSet = new HashSet<>();
    private Set<ReportingGroupPlanConfig> reportingGroupPlanConfigSet = new HashSet<>();
    private Set<ReportingGroupPlaybookConfig> reportingGroupPlaybookConfigSet = new HashSet<>();

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "CompanyID", length = 36)
    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    @Column(name = "GroupName", length = 30)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Column(name = "ParentGroupID", length = 36)
    public String getParentGroupID() {
        return parentGroupID;
    }

    public void setParentGroupID(String parentGroupID) {
        this.parentGroupID = parentGroupID;
    }

    @Column(name = "PrimaryLoginID", length = 36)
    public String getPrimaryLoginID() {
        return primaryLoginID;
    }

    public void setPrimaryLoginID(String primaryLoginID) {
        this.primaryLoginID = primaryLoginID;
    }

    @Column(name = "IntegratedPluginKey", length = 36)
    public String getIntegratedPluginKey() {
        return integratedPluginKey;
    }

    public void setIntegratedPluginKey(String integratedPluginKey) {
        this.integratedPluginKey = integratedPluginKey;
    }

    @Column(name = "CRMXREF", length = 100)
    public String getCRMXREF() {
        return CRMXREF;
    }

    public void setCRMXREF(String CRMXREF) {
        this.CRMXREF = CRMXREF;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reportinggroup")
    public Set<ReportingGroupMembership> getReportingGroupMembershipSet() {
        return reportingGroupMembershipSet;
    }

    public void setReportingGroupMembershipSet(Set<ReportingGroupMembership> reportingGroupMembershipSet) {
        this.reportingGroupMembershipSet = reportingGroupMembershipSet;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reportinggroup")
    public Set<ReportingGroupGridConfig> getReportingGroupGridConfigSet() {
        return reportingGroupGridConfigSet;
    }

    public void setReportingGroupGridConfigSet(Set<ReportingGroupGridConfig> reportingGroupGridConfigSet) {
        this.reportingGroupGridConfigSet = reportingGroupGridConfigSet;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reportinggroup")
    public Set<ReportingGroupMapConfig> getReportingGroupMapConfigSet() {
        return reportingGroupMapConfigSet;
    }

    public void setReportingGroupMapConfigSet(Set<ReportingGroupMapConfig> reportingGroupMapConfigSet) {
        this.reportingGroupMapConfigSet = reportingGroupMapConfigSet;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reportinggroup")
    public Set<ReportingGroupPlanConfig> getReportingGroupPlanConfigSet() {
        return reportingGroupPlanConfigSet;
    }

    public void setReportingGroupPlanConfigSet(Set<ReportingGroupPlanConfig> reportingGroupPlanConfigSet) {
        this.reportingGroupPlanConfigSet = reportingGroupPlanConfigSet;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reportinggroup")
    public Set<ReportingGroupPlaybookConfig> getReportingGroupPlaybookConfigSet() {
        return reportingGroupPlaybookConfigSet;
    }

    public void setReportingGroupPlaybookConfigSet(Set<ReportingGroupPlaybookConfig> reportingGroupPlaybookConfigSet) {
        this.reportingGroupPlaybookConfigSet = reportingGroupPlaybookConfigSet;
    }

}

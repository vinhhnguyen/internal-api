package model;

import java.util.Date;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "license", catalog = "revegyenterprise_4_0_qa")
public class License implements Serializable {
    public License() {
    }

    private String ID;
    private String companyID;
    private String licenseKey;
    private String purchaseCode;
    private java.util.Date expirationDate;
    private int numberOfLicense;
    private boolean mapConfigEnabled = true;
    private boolean planConfigEnabled = true;
    private boolean gridConfigEnabled = true;
    private boolean quotaConfigEnabled = true;
    private boolean playbookConfigEnabled = false;
    private boolean tabletEnabled = false;
    private boolean anychartAnalyticsEnabled = true;
    private boolean accountEnabled = true;
    private boolean opportunityEnabled = true;
    private boolean partnerEnabled = true;
    private boolean territoryEnabled = true;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "CompanyID", length = 32)
    public String getCompanyID() {
        return this.companyID;
    }

    public void setCompanyID(String companyId) {
        this.companyID = companyId;
    }

    @Column(name = "PurchaseCode", length = 128)
    public String getPurchaseCode() {
        return this.purchaseCode;
    }

    public void setPurchaseCode(String purchaseCode) {
        this.purchaseCode = purchaseCode;
    }

    @Column(name = "NumberOfLicense")
    public Integer getNumberOfLicense() {
        return this.numberOfLicense;
    }

    public void setNumberOfLicense(Integer numberOfLicense) {
        this.numberOfLicense = numberOfLicense;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ExpirationDate", length = 19)
    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Column(name = "mapConfigEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isMapConfigEnabled() {
        return this.mapConfigEnabled;
    }

    public void setMapConfigEnabled(boolean mapConfigEnabled) {
        this.mapConfigEnabled = mapConfigEnabled;
    }

    @Column(name = "planConfigEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPlanConfigEnabled() {
        return this.planConfigEnabled;
    }

    public void setPlanConfigEnabled(boolean planConfigEnabled) {
        this.planConfigEnabled = planConfigEnabled;
    }

    @Column(name = "gridConfigEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isGridConfigEnabled() {
        return this.gridConfigEnabled;
    }

    public void setGridConfigEnabled(boolean gridConfigEnabled) {
        this.gridConfigEnabled = gridConfigEnabled;
    }

    @Column(name = "quotaConfigEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isQuotaConfigEnabled() {
        return this.quotaConfigEnabled;
    }

    public void setQuotaConfigEnabled(boolean quotaConfigEnabled) {
        this.quotaConfigEnabled = quotaConfigEnabled;
    }

    @Column(name = "playbookConfigEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPlaybookConfigEnabled() {
        return this.playbookConfigEnabled;
    }

    public void setPlaybookConfigEnabled(boolean playbookConfigEnabled) {
        this.playbookConfigEnabled = playbookConfigEnabled;
    }

    @Column(name = "tabletEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isTabletEnabled() {
        return this.tabletEnabled;
    }

    public void setTabletEnabled(boolean tabletEnabled) {
        this.tabletEnabled = tabletEnabled;
    }

    @Column(name = "accountEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAccountEnabled() {
        return this.accountEnabled;
    }

    public void setAccountEnabled(boolean accountEnabled) {
        this.accountEnabled = accountEnabled;
    }

    @Column(name = "anychartAnalyticsEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAnychartAnalyticsEnabled() {
        return this.anychartAnalyticsEnabled;
    }

    public void setAnychartAnalyticsEnabled(boolean anychartAnalyticsEnabled) {
        this.anychartAnalyticsEnabled = anychartAnalyticsEnabled;
    }

    @Column(name = "opportunityEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isOpportunityEnabled() {
        return this.opportunityEnabled;
    }

    public void setOpportunityEnabled(boolean opportunityEnabled) {
        this.opportunityEnabled = opportunityEnabled;
    }

    @Column(name = "partnerEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPartnerEnabled() {
        return this.partnerEnabled;
    }

    public void setPartnerEnabled(boolean partnerEnabled) {
        this.partnerEnabled = partnerEnabled;
    }

    @Column(name = "territoryEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isTerritoryEnabled() {
        return this.territoryEnabled;
    }

    public void setTerritoryEnabled(boolean territoryEnabled) {
        this.territoryEnabled = territoryEnabled;
    }

    @Column(name = "Key", length = 512)
    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

}

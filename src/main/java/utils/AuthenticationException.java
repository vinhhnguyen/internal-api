package utils;

public class AuthenticationException extends Exception {

    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String username, String password) {
        this("Wrong username (" + username + ") or password (" + password + ")");
    }
}

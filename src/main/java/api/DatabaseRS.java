package api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import config.ServerConfigLoader;
import model.DatabaseServer;
import service.DatabaseService;

@Path("/server")
public class DatabaseRS {

    @Context
    private HttpServletRequest request;

    @POST
    @Path("switch")
    @Produces(MediaType.TEXT_PLAIN)
    public Response switchDatabaseServer(@FormParam("databaseName") String name) {
        try {
            new DatabaseService(request).switchDatabaseServer(name);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.ok("Successfully").build();
    }

    @GET
    @Path("current")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCurrentDatabaseServerAddress() {
        DatabaseServer rs = new DatabaseService(request).getCurrentDatabaseServer();
        if (rs != null) {
            return Response.ok(rs).build();
        } else
            return Response.status(500).entity("Please choose server first").type(MediaType.TEXT_PLAIN).build();
    }

    @GET
    @Path("list")
    public Response getServerList() {
        try {
            return Response.ok(ServerConfigLoader.getDatabaseList()).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

}

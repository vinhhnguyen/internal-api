package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import utils.CryptoUtil;

@Path("/license")
@Api("License APIs")
public class LicenseRS {
    @GET
    @Produces(value = { MediaType.TEXT_PLAIN })
    @ApiOperation(value = "Generate a license")
    public String getLicense(@QueryParam("numberOfEmployee") int quantity, @QueryParam("companyId") String companyId,
            @QueryParam("expirationDate") @ApiParam(defaultValue = "dd/mm/YYYY hh:mm:ss AM") String expirationDate) {
        try {
            // ED format: dd/mm/YYYY hh:mm:ss AM
            return CryptoUtil.encrypt("QTY=" + quantity + "|CID=" + companyId + "|ED=" + expirationDate);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}

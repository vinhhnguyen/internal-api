package service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import dao.LoginDAO;
import model.Contact;
import model.ContactId;
import model.ContactType;
import model.License;
import model.Login;
import model.ReportingGroup;
import model.ReportingGroupMembership;
import utils.CryptoUtil;
import utils.DataValidateException;
import utils.UnknownServerException;

public class LoginService extends BaseDBService {

    public LoginService(HttpServletRequest request) {
        super(request);
    }

    public LoginService(String currentDB) {
        super(currentDB);
    }

    public Login getLoginInfoById(String id) throws Exception {
        return new LoginDAO(currentDB).getLoginInfoById(id);
    }

    public List<Login> getLoginInfo(String username) throws Exception {
        List<Login> rs = null;
        rs = new LoginDAO(currentDB).getLoginInfoByLoginname(username);
        rs.stream().forEach(login -> {
            try {
                login.setPassword(CryptoUtil.decrypt(login.getPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return rs;
    }

    protected boolean isNullOrEmptyKey(String key) {
        if (key == null)
            return true;
        if (key.trim().equals(""))
            return true;
        if (key.equals("00000000-0000-0000-0000-000000000000"))
            return true;
        return false;
    }

    public Login update(Login login) throws Exception {
        LoginDAO loginDao = new LoginDAO(currentDB);
        Login currentLogin = loginDao.getLoginInfoById(login.getId());
        currentLogin.setType(login.getType());
        currentLogin.setAccountEnabled(login.isAccountEnabled());
        currentLogin.setOpportunityEnabled(login.isOpportunityEnabled());
        currentLogin.setPartnerEnabled(login.isPartnerEnabled());
        currentLogin.setPortfolioEnabled(login.isPortfolioEnabled());
        currentLogin.setAccessOverride(login.getAccessOverride());

        currentLogin = loginDao.update(currentLogin);
        return currentLogin;
    }

    public String addNewLogin(Login login, String userGroupId, int membershipType, String teamContactKey)
            throws Exception {
        String newID = null;
        try {
            login.setOwnerUsrKey("");
            login.setPassword(CryptoUtil.encrypt(login.getPassword()));
            if (login.getLoginGroupID() != null && login.getLoginGroupID().equals("")) {
                login.setLoginGroupID(null);
            }
            newID = add(login);
            login.setId(newID);
            if (login.getId() == null) {
                throw new Exception("The returned ID is null");
            }
            if (userGroupId != null && userGroupId.isEmpty()) {
                ReportingGroup group = new ReportingGroupService(currentDB).getShallowGroup(userGroupId);
                ReportingGroupMembership nrgm = new ReportingGroupMembership();
                nrgm.setLoginID(login.getId());
                nrgm.setType(membershipType);
                group.getReportingGroupMembershipSet().add(nrgm);
                new ReportingGroupService(currentDB).update(group);
            }
            if (teamContactKey.equals("-1")) {
                // no action is required
            } else if (teamContactKey.equals("0")) {
                Contact existingContact = null;
                List<Contact> matchingContacts = new ContactService(currentDB).listByEmail(login.getEmail(),
                        login.getCompanyId());
                if (matchingContacts != null && !matchingContacts.isEmpty()) {
                    for (Contact c : matchingContacts) {
                        if (c.getLoginID() != null && !"".equalsIgnoreCase(c.getLoginID())) {
                            continue;
                        }
                        if (c.getEmail().toLowerCase().equals(login.getEmail().toLowerCase())) {
                            existingContact = c;
                            break;
                        }
                    }
                }
                if (existingContact != null) {
                    existingContact.setLoginID(login.getId());

                    try {
                        Contact retContact = new ContactService(currentDB).update(existingContact,
                                login.getCompanyId());
                        if (retContact == null) {
                            throw new Exception("Team Member returned is null");
                        }
                    } catch (Exception ex) {
                        throw new Exception("Login was created.  Error updating team member: " + ex.getMessage());
                    }
                } else {
                    // create team contact
                    List<ContactType> contactTypes = new ContactTypeService(currentDB)
                            .getList(login.getCompanyId());
                    if (contactTypes == null || contactTypes.isEmpty()) {
                        throw new Exception("Unable to retrieve Team Member Types");
                    }

                    Contact c = new Contact();
                    c.setId(new ContactId());
                    c.getId().setCompanyId(login.getCompanyId());
                    c.setContactTypeKey(contactTypes.get(0).getId().getKey());
                    c.setTitle(login.getTitle());
                    c.setEmail(login.getEmail());
                    c.getId().setKey(java.util.UUID.randomUUID().toString());
                    c.setLoginID(login.getId());
                    c.setName(login.getFirstName() + " " + login.getLastName());
                    c.setPhone(login.getPhone());
                    c.setShortName(login.getShortName());

                    try {
                        Contact retContact = new ContactService(currentDB).add(c, login.getCompanyId(),
                                login.getLoginName());
                        if (retContact == null) {
                            throw new Exception("Team Member returned is null");
                        }
                    } catch (Exception ex) {
                        throw new Exception("Login was created.  Error adding team member: " + ex.getMessage());
                    }
                }
            } else {
                // associate with team contact
                Contact c = new ContactService(currentDB).get(teamContactKey, login.getCompanyId());
                if (c == null) {
                    throw new Exception("Unable to retrieve Team Member");
                }

                c.setLoginID(login.getId());

                try {
                    Contact retContact = new ContactService(currentDB).update(c, login.getCompanyId());
                    if (retContact == null) {
                        throw new Exception("Team Member returned is null");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new Exception("Login was created.  Error updating team member: " + ex.getMessage());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return login.getId();
    }

    private String add(Login login) throws Exception {
        validate(login);
        LoginDAO lDao = new LoginDAO(currentDB);
        int numberOfUsers = lDao.getNumberOfLogins(login.getCompanyId());
        LicenseService lBo = new LicenseService(currentDB);
        License license = lBo.getRecursive(login.getCompanyId());

        if (numberOfUsers >= license.getNumberOfLicense()) {
            throw new DataValidateException(
                    "All available users under this the current license agreement have been used.");
        }

        if (isNullOrEmptyKey(login.getOwnerUsrKey())) {
            login.setOwnerUsrKey(UUID.randomUUID().toString());
            login.setReassign(true);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        login.setPasswordSetDateTime(formatter.parse("2000/01/01"));

        // initializing to default values...if add login allows fields to be set, should
        // remove this
        Login initLogin = new Login();
        login.setSpellCheckLanguage(initLogin.getSpellCheckLanguage());
        login.setDefaultCurrencyKey(initLogin.getDefaultCurrencyKey());
        login.setShortDateFormat(initLogin.getShortDateFormat());
        login.setShortTimeFormat(initLogin.getShortTimeFormat());
        login.setReportOutputType(initLogin.getReportOutputType());
        login.setReportPasswordMode(initLogin.getReportPasswordMode());
        login.setPageUOMDisplay(initLogin.getPageUOMDisplay());
        login.setPageHeight(initLogin.getPageHeight());
        login.setPageWidth(initLogin.getPageWidth());
        login.setPageTopMargin(initLogin.getPageTopMargin());
        login.setPageBottomMargin(initLogin.getPageBottomMargin());
        login.setPageLeftMargin(initLogin.getPageLeftMargin());
        login.setPageRightMargin(initLogin.getPageRightMargin());
        login.setMapMinScale(initLogin.getMapMinScale());
        login.setSendTaskNotifications(initLogin.getSendTaskNotifications());
        login.setSendTaskReminders(initLogin.getSendTaskReminders());
        login.setSendTaskReminderDays(initLogin.getSendTaskReminderDays());
        login.setBestPracticePromptEnabled(initLogin.isBestPracticePromptEnabled());

        return new LoginDAO(currentDB).createNewUSer(login);
    }

    private void validate(Login login) throws Exception {
        LoginDAO loginDao = new LoginDAO(currentDB);

        List<Login> existingLogins = loginDao.listByNameCompanyID(login.getLoginName(), login.getCompanyId());
        if (existingLogins.size() > 0) {
            if (login.getId() == null) {
                throw new DataValidateException("Cannot have duplicate names");
            }
            for (Login existingLogin : existingLogins) {
                if (!login.getId().equals(existingLogin.getId())) {
                    throw new DataValidateException("Cannot have duplicate names");
                }
            }
        }
    }

}

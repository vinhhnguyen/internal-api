package service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import dao.ContactDAO;
import model.Contact;
import utils.DataValidateException;

public class ContactService extends BaseDBService {

    public ContactService(HttpServletRequest request) {
        super(request);
    }

    public ContactService(String currentDB) {
        super(currentDB);
    }

    public List<Contact> getContactList(String companyID) throws Exception {
        return new ContactDAO(currentDB).listByCompanyID(companyID);
    }

    public List<Contact> getUnassignedContacts(String companyID) throws Exception {
        List<Contact> contacts = getContactList(companyID);
        List<Contact> unassignedContacts = new ArrayList<>();
        for (Contact c : contacts) {
            if (c.getLoginID() == null || c.getLoginID().equals("")) {
                unassignedContacts.add(c);
            }
        }
        Collections.sort(unassignedContacts, new contactNameComparer());
        return unassignedContacts;
    }

    public Contact add(Contact contact, String companyID, String loginName) throws Exception {
        contact.getId().setCompanyId(companyID);
        ContactDAO contactDao = new ContactDAO(currentDB);
        List<Contact> existingContacts = contactDao.listByNameCompanyID(contact.getName(),
                contact.getId().getCompanyId());
        if (existingContacts.size() > 0) {
            for (Contact existingContact : existingContacts) {
                if ((isNullOrBlank(contact.getEmail()) ? "" : contact.getEmail()).equalsIgnoreCase(
                        isNullOrBlank(existingContact.getEmail()) ? "" : existingContact.getEmail())) {
                    System.err.println("Attempt to add duplicate contact name/email: " + contact.getName() + " / "
                            + contact.getEmail());
                    throw new DataValidateException("Cannot have duplicate team members (" + contact.getName() + " / "
                            + contact.getEmail() + ")");
                }
            }
        }
        contact.setDeleted(false);
        contact.setLastUpdated(new java.util.Date(System.currentTimeMillis()));
        contact.setLastUpdatedBy(loginName);
        return contactDao.insert(contact);
    }

    public Contact get(String key, String companyID) throws Exception {
        return new ContactDAO(currentDB).get(key, companyID);
    }

    private class contactNameComparer implements Comparator {

        public int compare(Object o1, Object o2) {
            if (o1 instanceof Contact && o2 instanceof Contact) {
                Contact an1 = (Contact) o1;
                Contact an2 = (Contact) o2;

                int ncv = an1.getName().compareToIgnoreCase(an2.getName());

                if (ncv != 0) {
                    return ncv;
                }

                return an1.getId().getKey().compareTo(an2.getId().getKey());
            }
            return 0;
        }
    }

    public List<Contact> listByEmail(String email, String companyID) throws Exception {
        if (isNullOrBlank(email))
            return null;
        return new ContactDAO(currentDB).findByEmail(email, companyID);
    }

    public Contact update(Contact contact, String companyID) throws Exception {
        ContactDAO contactDao = new ContactDAO(currentDB);

        List<Contact> existingContacts = contactDao.listByNameCompanyID(contact.getName(), companyID);
        if (existingContacts.size() > 0) {
            for (Contact existingContact : existingContacts) {
                if (!contact.getId().getKey().equals(existingContact.getId().getKey())
                        && (isNullOrBlank(contact.getEmail()) ? "" : contact.getEmail()).equalsIgnoreCase(
                                isNullOrBlank(existingContact.getEmail()) ? "" : existingContact.getEmail())) {
                    System.err.println("Attempt to update duplicate contact name/email: " + contact.getName() + " / "
                            + contact.getEmail());
                    throw new DataValidateException("Cannot have duplicate team members (" + contact.getName() + " / "
                            + contact.getEmail() + ")");
                }
            }
        }

        Contact currentContact = contactDao.get(contact.getId().getKey(), companyID);
        currentContact.setName(contact.getName());
        currentContact.setShortName(contact.getShortName());
        currentContact.setContactTypeKey(contact.getContactTypeKey());
        currentContact.setTitle(contact.getTitle());
        currentContact.setEmail(contact.getEmail());
        currentContact.setPhone(contact.getPhone());
        currentContact.setMobile(contact.getMobile());
        currentContact.setCompany(contact.getCompany());
        currentContact.setLoginID(contact.getLoginID());
        currentContact.setDeleted(contact.isDeleted());
        currentContact.setUserDefinedField1(contact.getUserDefinedField1());
        currentContact.setUserDefinedField2(contact.getUserDefinedField2());
        currentContact.setCRMXREF(contact.getCRMXREF());

        currentContact.setLastUpdated(new java.util.Date(System.currentTimeMillis()));
        currentContact.setLastUpdatedBy(null);

        return contactDao.update(currentContact);
    }

    protected boolean isNullOrBlank(String val) {
        if (val == null)
            return true;
        if (val.trim().equals(""))
            return true;
        return false;
    }
}

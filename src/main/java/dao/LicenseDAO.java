package dao;

import java.util.List;

import model.License;

public final class LicenseDAO extends DAO {

    public LicenseDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public License getByCompanyId(String companyId) throws Exception {
        try {
            List<License> list = getSessionFactory().openSession()
                    .createQuery("FROM License WHERE companyID = '" + companyId + "'").list();
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            throw new Exception("Error while attempting to get License company ID " + companyId);
        } finally {
            closeSession();
        }
    }

    public List<License> list() throws Exception {
        try {
            return getSessionFactory().openSession().createQuery("FROM License").list();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            throw new Exception("Error while attempting to get list of Licenses");
        } finally {
            closeSession();
        }
    }
}

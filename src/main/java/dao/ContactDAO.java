package dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import model.Contact;

public final class ContactDAO extends DAO {
    public ContactDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public Contact insert(Contact contact) throws Exception {
        try {
            begin();
            getSession().save(contact);
            commit();
            return contact;
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

    public List<Contact> listByCompanyID(String companyID) throws Exception {
        try {
            return getSession().createQuery("FROM Contact WHERE companyID='" + companyID + "'").list();
        } finally {
            closeSession();
        }
    }

    public List<Contact> findByEmail(String email, String companyId) throws Exception {
        try {
            DetachedCriteria accts = DetachedCriteria.forClass(Contact.class)
                    .add(Restrictions.ilike("email", email, MatchMode.EXACT))
                    .add(Restrictions.eq("id.companyId", companyId));
            return accts.getExecutableCriteria(getSession()).list();
        } finally {
            closeSession();
        }
    }

    public List<Contact> listByNameCompanyID(String name, String companyID) throws Exception {
        try {
            return getSession()
                    .createQuery("FROM Contact c WHERE lower(c.name)='" + name.toLowerCase().replaceAll("'", "''")
                            + "' AND deleted=false AND c.id.companyId='" + companyID + "'")
                    .list();
        } finally {
            closeSession();
        }
    }

    public Contact get(String key, String companyID) throws Exception {
        try {
            List<Contact> list = getSession()
                    .createQuery("FROM Contact c WHERE c.id.key = :key AND c.id.companyId = :companyId")
                    .setString("key", key).setString("companyId", companyID).list();
            return list.get(0);
        } finally {
            closeSession();
        }
    }

    public Contact update(Contact contact) throws Exception {
        try {
            begin();
            getSession().update(contact);
            commit();
            return contact;
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

}

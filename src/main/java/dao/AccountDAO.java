package dao;
// default package

// Generated Jan 22, 2018 4:40:01 PM by Hibernate Tools 5.2.6.Final

import java.util.List;
import org.hibernate.query.Query;

import model.Account;

/**
 * Home object for domain model class Account.
 * 
 * @see .Account
 * @author Hibernate Tools
 */

public class AccountDAO extends DAO {

    public AccountDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public List<Account> getAllAccount() throws Exception {
        List<Account> rs = null;
        try {
            session = getSessionFactory().openSession();
            session.beginTransaction();
            String HQL = "from Account";
            Query<Account> query = session.createQuery(HQL);
            rs = query.getResultList();
        } finally {
            closeSession();
        }
        return rs;
    }

}

package dao;

import java.util.List;
import javax.persistence.Query;
import model.LoginRole;

public final class LoginRoleDAO extends DAO {
    public LoginRoleDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public List<LoginRole> listByCompanyID(String companyID) throws Exception {
        try {
            String hql = "from LoginRole where CompanyID=?";
            Query query = getSession().createQuery(hql);
            query.setParameter(0, companyID);
            return query.getResultList();
        } finally {
            closeSession();
        }
    }
}

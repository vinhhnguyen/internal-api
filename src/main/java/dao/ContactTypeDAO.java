package dao;

import java.util.List;

import model.ContactType;

public final class ContactTypeDAO extends DAO {
    public ContactTypeDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public ContactType insert(ContactType contactType) throws Exception {
        try {
            begin();
            getSession().save(contactType);
            commit();
            return contactType;
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

    public ContactType update(ContactType contactType) throws Exception {
        try {
            begin();
            getSession().update(contactType);
            commit();
            return contactType;
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

    public void delete(ContactType contactType) throws Exception {
        try {
            begin();
            getSession().delete(contactType);
            commit();
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

    public ContactType get(String key, String companyID) throws Exception {
        try {
            List<ContactType> list = getSession()
                    .createQuery("FROM ContactType WHERE id.key = '" + key + "' AND id.companyId='" + companyID + "'")
                    .list();
            return list.get(0);
        } finally {
            closeSession();
        }
    }

    public List<ContactType> listByCodeOrNameCompanyID(String code, String name, String companyID) throws Exception {
        try {
            return getSession()
                    .createQuery("FROM ContactType WHERE (code = '" + code.replaceAll("'", "''") + "' OR name = '"
                            + name.replaceAll("'", "''") + "') AND id.companyId='" + companyID + "' ORDER BY sortOrder")
                    .list();
        } finally {
            closeSession();
        }
    }

    public List<ContactType> listByCompanyID(String companyID) throws Exception {
        try {
            return getSession()
                    .createQuery("FROM ContactType WHERE id.companyId='" + companyID + "' ORDER BY sortOrder").list();
        } finally {
            closeSession();
        }
    }

}

package config;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.BeanConfig;

/**
 * Servlet implementation class SwaggerConfiguration
 */
public class SwaggerConfig extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("Internal API");
        beanConfig.setVersion("1.0");
        beanConfig.setSchemes(new String[] { "http" });
        beanConfig.setBasePath("/revegy-api/rest");
        beanConfig.setResourcePackage("api");
        beanConfig.setScan(true);
        beanConfig.setDescription("Revegy team internal APIs");

    }
}

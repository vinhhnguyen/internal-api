package config;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.DatabaseServer;

public class ServerConfigLoader {
    private static Map<String, DatabaseServer> databaseMap;
    private static final String fileName = "database-list.json";
    static {
        databaseMap = new HashMap<>();
        new ServerConfigLoader().loadDatabaseServers();
    }

    public void loadDatabaseServers() {
        ObjectMapper om = new ObjectMapper();
        DatabaseServer[] databaseList = null;
        try (InputStream is = ServerConfigLoader.class.getClassLoader().getResourceAsStream(fileName);) {
            databaseList = om.readValue(is, DatabaseServer[].class);
            Arrays.stream(databaseList).forEach(database -> databaseMap.put(database.getDatabaseName(), database));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isDatabaseExisted(String name) {
        return databaseMap.get(name) != null;
    }

    public static List<DatabaseServer> getDatabaseList() {
        return new ArrayList<>(databaseMap.values());
    }

    public static DatabaseServer getDatabase(String name) {
        return databaseMap.get(name);
    }

}

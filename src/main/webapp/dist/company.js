$(function() {
    $(getV4Form()).submit(function(e) {
        e.preventDefault();
    });
});
var companiesInfo;
function getV4Form() {
    return $('#V4Form');
}

function updateV4CompanyList() {
    var form = getV4Form();
    var alertTag = $(form).find(".http-status").first();
    alertTag.hide();
    $.ajax({
        url: companyList_URL,
        type: 'GET',
        success: function(data) {
            companiesInfo = data;
            var companyList = $(form).find("[name=companyID]").first();
            companyList.html('<option value="0" selected="selected"></option>');
            for (i = 0; i < data.length; i++) {
                companyList.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
            }
            mapCompanyV4();
        },
        error: function(response) {
            alertTag.prop('color', 'red');
            alertTag.text(response.responseText);
            alertTag.show();
        }
    });
}

function mapCompanyV4() {
    var form = getV4Form();
    var id = getFirstAttrByName(form, 'companyID');
    if (id !== '0') {
        for (i = 0; i < companiesInfo.length; i++) {
            if (companiesInfo[i].id === id) {
                setFirstAttrByName(form, 'status', companiesInfo[i].v4enabled);
                break;
            }
        }
    }
}

function updateV4() {
    var form = getV4Form();
    var statusTag = $(getV4Form()).find('.http-status:nth-child(2)');
    var companyID = getFirstAttrByName(form, 'companyID');
    var v4Enabled = getFirstAttrByName(form, 'status');
    $.ajax({
        url: updateCompanyV4_URL,
        type: 'POST',
        data: {companyId: companyID, v4Enabled: v4Enabled},
        success: function(data) {
            statusTag.text('Successfully');
            statusTag.prop('color', 'green');
        }, error: function(response) {
            statusTag.text(response.responseText);
            statusTag.prop('color', 'red');
        }
    });
}